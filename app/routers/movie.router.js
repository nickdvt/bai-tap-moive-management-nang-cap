const express = require('express');
const { logger } = require('../middlewares/logger.middleware');
const { checkExist } = require('../middlewares/validations.middleware');
const {User} = require("../../models")
const movieRouter = express.Router();
const { getListMovie, getDetailMovieByID, createNewMovie, updateMovieByID, deleteMovieByID } = require('./../controllers/movie.controller');


// Lấy danh sách phim
movieRouter.get('/',logger("Lấy tất cả người dùng"), getListMovie)

// Lấy chi tiết phim
movieRouter.get('/:id',logger("Lấy chi tiết người dùng theo id"),getDetailMovieByID);

// Thêm phim
movieRouter.post('/',createNewMovie);

// Cập nhật phim
movieRouter.put('/:id',checkExist(User),updateMovieByID);

// Xoá phim
movieRouter.delete('/:id',checkExist(User),deleteMovieByID);

module.exports = movieRouter;