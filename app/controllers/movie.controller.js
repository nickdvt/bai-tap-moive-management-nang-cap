const { User } = require("../../models")

const getListMovie = async (req, res) => {
    try {
        const listMovie = await User.findAll();
        res.status(200).send(listMovie);
    } catch (error) {
        res.status(500).send(error);
    }
};

const getDetailMovieByID = async (req, res) => {
    const { id } = req.params;
    try {
        const movieDetail = await User.findByPk(id);
        if(movieDetail){
            res.status(200).send(movieDetail)
        }else{
            res.status(404).send("Not Found!");
        }
    } catch (error) {
        res.status(500).send(error);
    }
};

const createNewMovie = async (req, res) => {
    const { name, totalMovieTime, poster, trailer } = req.body;
    try {
        const newMovie = await User.create({ name, totalMovieTime, poster, trailer });
        res.status(201).send(newMovie);
    } catch (error) {
        res.status(500).send(error);
    }
};

const updateMovieByID = async (req, res) => {
    const { id } = req.params;
    const { name, totalMovieTime, poster, trailer } = req.body;
    try {
        const [countUpdate] = await User.update(
            { name, totalMovieTime, poster, trailer },
            {
                where: {
                    id,
                }
            }
        );
        if (countUpdate > 0) {
            res.status(200).send("Update Success.");
        } else {
            res.status(404).send("Not Found!");
        }
    } catch (error) {
        res.status(500).send(error);
    }
};

const deleteMovieByID = async (req, res) => {
    const { id } = req.params;
    try {
        await User.destroy({
            where: {
                id,
            },
        });
        res.status(200).send("Xóa Thành Công");
    } catch (error) {
        res.status(500).send(error);
    }
};

module.exports = {
    getListMovie,
    getDetailMovieByID,
    createNewMovie,
    updateMovieByID,
    deleteMovieByID
}