const logger = (message) => (req, res, next) => {
    console.log(message);
    next();
  };
  
  module.exports = {
    logger,
  };